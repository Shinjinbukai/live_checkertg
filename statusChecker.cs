using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace domain_checker
{
    public class statusChecker
    {
        public List<domainObject> _domainObject { get; set; }
        public string _chatId { get; set; }

        public int _interval { get; set; }

        public statusChecker()
        {
            _domainObject = new List<domainObject>();
            new Thread(async () => 
            {
                Thread.CurrentThread.IsBackground = true; 
                /* run your code here */ 
                Console.WriteLine("Telegram checker running...");
                await _startStatusChecker();
            }).Start();
        }

        private async Task _startStatusChecker()
        {
            while (true)
            {
                try
                {
                    if (_domainObject.Count > 0)
                    {
                        foreach (var domainObject in _domainObject)
                        {
                            if (Checkstatus(domainObject))
                            {
                                domainObject.Status = "Online";
                            }
                            else
                            {
                                domainObject.Status = "Offline";
                                await Program.Bot.SendTextMessageAsync(_chatId, $"!DOMAIN {domainObject.Domain} DOWN!");
                            }
                        }    
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                Thread.Sleep(_interval);
            }
        }

        private static bool Checkstatus(domainObject _domain)
        {
            try
            {
                var tmpWebResponse = GetWebContents(_domain).ToLower();
                string[] expectedResponse = _domain.Expected;
                foreach (var exRS in expectedResponse)
                {
                    return tmpWebResponse.Contains(exRS);
                }

                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private static string GetWebContents(domainObject _domain)
        {
            try
            {
                using (var client = new WebClient())
                {
                    return client.DownloadString(_domain.Domain);
                }
            }
            catch (Exception e)
            {
                return "Error; " + e.Message;
            }
        }
    }
}