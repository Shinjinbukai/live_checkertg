﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;

namespace domain_checker
{
    class Program
    {
        public static readonly TelegramBotClient Bot = 
            new TelegramBotClient("768431757:AAFzwDdWJ8v54uyc6Oobs8Q_3bySBJ3GGFc");
        private static List<statusChecker> _statusChecker = new List<statusChecker>();
        
        static void Main(string[] args)
        {
            var me = Bot.GetMeAsync().Result;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;
            enableBot();
            Console.ReadLine();
            stopBot();
            Console.ReadLine();
        }

        private static void enableBot()
        {
            Bot.StartReceiving(Array.Empty<UpdateType>());
            Console.WriteLine($"Started the bot.");
        }
        private static void stopBot(){
            Bot.StopReceiving();
        }
        
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.Text) return;

            //Console.WriteLine(message.Chat.Id);

            switch (message.Text.Split(' ').First())
            {
                case "/init":
                        int interval = 10000;
                        var tmpFile = message.Text.Split(' ');
                        if (tmpFile.Length > 1)
                        {
                            if (isNumeric(tmpFile[1]))
                            {
                                interval = Convert.ToInt32(message.Text.Split(' ')[1]);
                            }
                        }
                        
                        if (_statusChecker.Exists(p => p._chatId == message.Chat.Id.ToString())) break;
                        _statusChecker.Add(new statusChecker
                        {
                            _interval = interval,
                            _domainObject = new List<domainObject>(),
                            _chatId = message.Chat.Id.ToString()
                        });
                    break;
                // track domain
                case "/add":
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    var domainToAdd = message.Text.Split(' ')[1];
                    if (_statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.Exists(p => p.Domain.Contains(domainToAdd)))
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, $"Domain already exist @Index {_statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.FindIndex(p => p.Domain == domainToAdd)}");
                        break;
                    }

                    
                    string[] filteredHeaders = {domainToAdd};
                    var tmpString = message.Text.Split(new string[] { domainToAdd }, StringSplitOptions.None)[1];
                    if (tmpString.Contains(","))
                    {
                        filteredHeaders = tmpString.Split(',');
                    }
                    
                    if (!domainToAdd.StartsWith("http")) domainToAdd = $"https://{domainToAdd}";
                    _statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.Add(new domainObject
                    {
                        Domain = domainToAdd, 
                        Expected = filteredHeaders
                    });
                    
                    int domainToAddIndex = _statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.FindIndex(p => p.Domain == domainToAdd);
                    await Bot.SendTextMessageAsync(message.Chat.Id, $"Added {domainToAdd}({domainToAddIndex}) to the tracking list;");
                    break;

                // delete domain
                case "/delete":
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    var domainToDel = message.Text.Split(' ')[1];
                    if (!_statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.Exists(p => p.Domain.Contains(domainToDel)))
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, $"Domain {domainToDel} does not exist;");
                        break;
                    }
                    int domainToDelIndex = _statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.FindIndex(p => p.Domain.Contains(domainToDel));
                    _statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.RemoveAt(domainToDelIndex);
                    await Bot.SendTextMessageAsync(message.Chat.Id, $"Deleted {domainToDel}({domainToDelIndex}) from the tracking list;");
                    break;
                
                case "/status":
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    var tmpResponse = "";
                    if (_statusChecker.Any(p => p._chatId == message.Chat.Id.ToString()))
                    {
                        foreach (var tmpObject in _statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject)
                        {
                            tmpResponse = tmpResponse + $"\n//({_statusChecker.FirstOrDefault(p => p._chatId == message.Chat.Id.ToString())._domainObject.FindIndex(p => p.Domain == tmpObject.Domain)})Domain:{tmpObject.Domain} \n-Status:{tmpObject.Status}\n";
                        }
                    }

                    await Bot.SendTextMessageAsync(message.Chat.Id, $"list{tmpResponse};");
                
                    break;
                
                default:
                    const string usage = @"
Usage:
/init - Init the bot; Params: [interval in ms] optional, default is 10000;
/add domain.com mysuperduperstringthatisexpectedintheresponse - add domain check for response strings seperated by <,>
/delete domain - delete domain by name
/status - report all statuses";

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        usage,
                        replyMarkup: new ReplyKeyboardRemove());
                    break;
            }
        }

        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await Bot.AnswerCallbackQueryAsync(
                callbackQuery.Id,
                $"Received {callbackQuery.Data}");

            await Bot.SendTextMessageAsync(
                callbackQuery.Message.Chat.Id,
                $"Received {callbackQuery.Data}");
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
        
        private static bool isNumeric(string str)
        {
            var isNumeric = int.TryParse(str, out int n);
            return isNumeric;
        }
    }
}
