using System.Net.Http.Headers;

namespace domain_checker
{
    public class domainObject
    {
        public string Domain { get; set; }
        public string[] Expected { get; set; }
        public HttpHeaders ExpectedHeaders { get; set; }
        public string Status { get; set; }
    }
}